package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/jwtauth"
	"github.com/qor/admin"
	"github.com/qor/publish2"
	"github.com/qor/qor"
	"github.com/qor/qor/utils"

	"bitbucket.org/tramascrew/qor-blog/config/bindatafs"
	_ "bitbucket.org/tramascrew/qor-blog/config/db/migrations"

	"bitbucket.org/tramascrew/qor-blog/app/account"
	adminapp "bitbucket.org/tramascrew/qor-blog/app/admin"
	"bitbucket.org/tramascrew/qor-blog/app/api"
	"bitbucket.org/tramascrew/qor-blog/app/pages"
	"bitbucket.org/tramascrew/qor-blog/app/static"
	"bitbucket.org/tramascrew/qor-blog/config"
	"bitbucket.org/tramascrew/qor-blog/config/application"
	"bitbucket.org/tramascrew/qor-blog/config/auth"
	"bitbucket.org/tramascrew/qor-blog/config/db"
	"bitbucket.org/tramascrew/qor-blog/utils/funcmapmaker"
)

func main() {
	// Check if the --compile-templates flag has been added when executing
	// the file. If it is it will compile the templates into the binary,
	// instead of running the server.
	cmdLine := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
	compileTemplate := cmdLine.Bool("compile-templates", false, "Compile Templates")
	startPublicServer := cmdLine.Bool("public", false, "Public")
	startAuthenticatedServer := cmdLine.Bool("auth", false, "Auth")
	cmdLine.Parse(os.Args[1:])

	if !*compileTemplate && !*startPublicServer && !*startAuthenticatedServer {
		fmt.Println("No parameters passed, use 'public', 'auth' or 'compile-templates'")
		return
	}

	// Initializes the core components of the application:
	//	- Router
	//	- Admin: the admin dashboard / backoffice
	//	- Application: the frontend + backoffice
	// Note that the Application uses the Admin to setup the glue between
	// the frontend and the backend. Resources will be added directly
	// to the Application object.
	var (
		RouterPublic = chi.NewRouter()
		RouterAuth   = chi.NewRouter()
		Admin        = admin.New(&admin.AdminConfig{
			SiteName: "QOR DEMO",
			Auth:     auth.AdminAuth{},
			DB:       db.DB.Set(publish2.VisibleMode, publish2.ModeOff).Set(publish2.ScheduleMode, publish2.ModeOff),
		})
		PublicApp = application.New(&application.Config{
			Router: RouterPublic,
			Admin:  Admin,
			DB:     db.DB,
		})
		AuthApp = application.New(&application.Config{
			Router: RouterAuth,
			Admin:  Admin,
			DB:     db.DB,
		})
	)

	// Add the admin views to the funcmap.
	// The views are located in /app/views/auth, as configured
	// in /config/auth/auth.go
	funcmapmaker.AddFuncMapMaker(auth.Auth.Config.Render)

	if *startPublicServer {
		servePublicApp(RouterPublic, PublicApp)
	} else if *startAuthenticatedServer {
		serveAuthApp(RouterAuth, AuthApp)
	} else if *compileTemplate {
		bindatafs.AssetFS.Compile()
	}
}

func servePublicApp(r *chi.Mux, app *application.Application) {
	setDefaultMiddlewares(r)

	app.Use(adminapp.New(&adminapp.Config{}))
	app.Use(account.New(&account.Config{}))
	app.Use(pages.New(&pages.Config{}))

	// Configure the static folders, routing to custom paths
	app.Use(static.New(&static.Config{
		Prefixs: []string{"/system"},
		Handler: utils.FileServer(http.Dir(filepath.Join(config.Root, "public"))),
	}))
	app.Use(static.New(&static.Config{
		Prefixs: []string{"javascripts", "stylesheets", "images", "dist", "fonts", "vendors"},
		Handler: bindatafs.AssetFS.FileServer(http.Dir("public"), "javascripts", "stylesheets", "images", "dist", "fonts", "vendors"),
	}))

	serveApp(app, config.Config.PortPublic)

}

func serveAuthApp(r *chi.Mux, app *application.Application) {
	setDefaultMiddlewares(r)

	fmt.Println(config.Config.JWTSecret)
	r.Use(jwtauth.Verifier(jwtauth.New("HS256", []byte(config.Config.JWTSecret), nil)))
	r.Use(jwtauth.Authenticator)

	app.Use(api.New(&api.Config{}))

	serveApp(app, config.Config.PortAuth)
}

func setDefaultMiddlewares(r *chi.Mux) {
	// Setup the router middlewares. Check their documentation (f12)
	// to see more details about each middleware.
	r.Use(func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			// for demo, don't use this for your production site
			w.Header().Add("Access-Control-Allow-Origin", "*")
			handler.ServeHTTP(w, req)
		})
	})

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.DefaultCompress)
	r.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			var (
				tx         = db.DB
				qorContext = &qor.Context{Request: req, Writer: w}
			)

			if locale := utils.GetLocale(qorContext); locale != "" {
				tx = tx.Set("l10n:locale", locale)
			}

			ctx := context.WithValue(req.Context(), utils.ContextDBName, publish2.PreviewByDB(tx, qorContext))
			next.ServeHTTP(w, req.WithContext(ctx))
		})
	})
}

func serveApp(app *application.Application, port uint) {
	fmt.Printf("Listening on: %v\n", port)
	if config.Config.HTTPS {
		if err := http.ListenAndServeTLS(fmt.Sprintf(":%d", port), "config/local_certs/server.crt", "config/local_certs/server.key", app.NewServeMux()); err != nil {
			panic(err)
		}
	} else {
		if err := http.ListenAndServe(fmt.Sprintf(":%d", port), app.NewServeMux()); err != nil {
			panic(err)
		}
	}
}
