package migrations

import (
	"github.com/qor/activity"
	"github.com/qor/auth/auth_identity"
	"github.com/qor/banner_editor"
	"github.com/qor/help"
	"github.com/qor/media/asset_manager"

	"bitbucket.org/tramascrew/qor-blog/config/db"
	"bitbucket.org/tramascrew/qor-blog/models/blogs"
	"bitbucket.org/tramascrew/qor-blog/models/seo"
	"bitbucket.org/tramascrew/qor-blog/models/settings"
	"bitbucket.org/tramascrew/qor-blog/models/users"
)

func init() {
	AutoMigrate(&auth_identity.AuthIdentity{})
	AutoMigrate(&asset_manager.AssetManager{})
	AutoMigrate(&settings.MediaLibrary{})

	AutoMigrate(&users.User{})

	AutoMigrate(&seo.SiteSEOSetting{})
	AutoMigrate(&blogs.Page{}, &blogs.Post{})

	AutoMigrate(&activity.QorActivity{})
	AutoMigrate(&help.QorHelpEntry{})
	AutoMigrate(&banner_editor.QorBannerEditorSetting{})
}

// AutoMigrate run auto migration
func AutoMigrate(values ...interface{}) {
	for _, value := range values {
		db.DB.AutoMigrate(value)
	}
}
