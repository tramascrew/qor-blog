package auth

import (
	"fmt"
	"net/http"

	"bitbucket.org/tramascrew/qor-blog/models/users"
	"github.com/qor/admin"
	"github.com/qor/qor"
	"github.com/qor/roles"
)

func init() {
	roles.Register("editor", func(req *http.Request, currentUser interface{}) bool {
		return currentUser != nil && currentUser.(*users.User).Role == "Editor"
	})
}

type EditorAuth struct {
}

func (EditorAuth) LoginURL(c *admin.Context) string {
	return "/auth/login"
}

func (EditorAuth) LogoutURL(c *admin.Context) string {
	return "/auth/logout"
}

func (EditorAuth) GetCurrentUser(c *admin.Context) qor.CurrentUser {
	currentUser := Auth.GetCurrentUser(c.Request)
	if currentUser != nil {
		qorCurrentUser, ok := currentUser.(qor.CurrentUser)
		if !ok {
			fmt.Printf("User %#v haven't implement qor.CurrentUser interface\n", currentUser)
		}
		return qorCurrentUser
	}
	return nil
}
