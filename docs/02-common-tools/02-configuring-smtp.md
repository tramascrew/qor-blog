## Configuring The SMTP
Official docs: [smtp](https://github.com/qor/doc/blob/master/guides/sending_emails.md)

You can find the SMTP configuration in the config/config.go file.

For examples on how to use it, check the official docs.