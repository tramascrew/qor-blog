## RESTFul API
* Official docs: [restful api](https://github.com/qor/doc/blob/master/admin/restful_api.md)

### Adding Resources To The API
This can be easily done adding the models you want to be accessible through the API in the `/app/api/api.go` file.

### Customizing The Model's Properties Exposed In The API
Check the example from the docs.

QUESTION: difference between IndexAttrs and ShowAttrs?

### Using Actions Through API Endpoints
Check the example from the docs.

### Enabling Authentication
The official docs is not very clear in this example, so let me show how would you enable authentication for the API:

Instead of using the qor.Config{} struct, you need to use the AdminConfig{}, which has the Auth property:

``` go
// no auth
API := admin.New(&qor.Config{
    DB: db.DB,
})

// auth enabled
API := admin.New(&qor.AdminConfig{
    DB: db.DB,
    Auth: auth.AdminAuth{},
})
```

And then, when adding new resources, you need to assign the desired Authorization level:

``` go
// no auth
API.AddResource(&users.User{})

// auth enabled with sample permission schema
API.AddResource(&users.User{}, &admin.Config{
    Permission: roles.
        Deny(roles.Delete, roles.Anyone).
        Allow(roles.Delete, "admin"),
})
```