## Built-in Cache Solutions
* Official docs: [cache](https://github.com/qor/doc/blob/master/guides/caching.md)

QOR provides several wrappers of the most commonly used cache engines ready to use.

In the official docs you will find how to use each one.

TODO: add implementation to current project to cache blog posts, for example