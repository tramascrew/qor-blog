## Config folder

### Adding New User Roles
* Official docs: [Auth](https://github.com/qor/doc/blob/master/admin/authentication.md)

The auth package contains the core file `auth.go` that setups the authentication and authority features.

In this file you can also configure the database, the renderer and the Redirector.

Now, to manage the types of users that can be registered in your application, you must create a new file following the `admin_auth.go` example.

This new file must call the `roles.Register(...)` function, where you can define the name of the new role. You will also need to implement the `LoginURL()`, `LogoutURL()` and `GetCurrentUser()` methods.

### Internationalization & Localization
* Official docs: [i18n](https://github.com/qor/doc/blob/master/guides/i18n.md), [l10n](https://github.com/qor/doc/blob/master/guides/l10n.md)

The internationalization (i18n) is used to translate pieces of content (for example, the "Log in" text).

On the other hand, the localization (l10n) is used to create different models variants, one for each supported language.

### Using The BindataFS
* Official docs: [bindatafs](https://github.com/qor/doc/blob/master/guides/bindatafs.md), [qor-render](https://github.com/qor/render)

The ``bindatafs`` folder includes an implementation of the QOR's assetFS package, which is used to load templates and compile them into the application's binary.

You don't really have to configure it any further.

The most important thing you must know is the `(application|bindatafs).AssetFS.NameSpace("name")` where `name` is the folder containing the views, generally located within the `app` folder.

You will commonly assing the AssetFS result to a [Render](https://github.com/qor/render) instance

### Database Migrations
One of the most important features you should enable from the beginning is the database migrations.

The migrations are managed in the `config/db/migrations/migration.go` file.

With QOR this is easily handled: just call the `AutoMigrate(your.model)` passing your model/s and you are done.

This function will create/update the tables according to the models structure.

### Getting The config.go Ready
* config.go: 
    * modify the Root variable to point your project path
    * setup the smtp functionlaity (check out the Mailer usage at the end of the file)
* *.yml files: modify the configuration files accordingly