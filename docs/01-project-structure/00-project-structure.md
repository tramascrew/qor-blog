# Project Structure
* Official docs: [doc](https://github.com/qor/doc), (qor)[https://github.com/qor/qor]

The first thing we need to understand is the project structure. Why? Because this project, although simple, has been written to easily add new components as it grows, and this reason alone is more than enough to follow a scalable structure rather than the typicall demo structure.

As this project aims to be a starting point for several CMS related projects (as well as a tutorial), I consider that it can't be written following a dumb structure.

That being said, let's start taking a look at the program's entry point: the root `main.go` file.

## Overview
### main.go
This is the entry point of the application. It can be run in two different ways:
* `go run main.go` to run the application normally
* `go run main.go --compile-templates` to compile the templates into the binary

### config/*
In this folder we find different config files to setup different core parts of the application, such as:
* database
* auth
* internationalization

### app/*
In this folder we find the business logic of the application, organized by components. Some of the common components that we can find in every QOR application are:
* admin: to manage the admin dashboard
* api: where you setup the models available through the api
* account: to handle users' data
* static: to manage the static pages/assets of your application
* views: ¿?

### public/*
The public folder contains the assets (css, javascript, images) used in the frontend or to render the admin dashboard.

### models/*
This is, as the name suggests, the folder where the models are stored, also organized by components.

Expect to find only structs and no logic.