## Exploring the entry point: main.go

### Reading The Parameter From The Command Line

``` go
// Check if the --compile-templates parameter has been added when executing
// the file. If it is it will compile the templates into the binary,
// instead of running the server.
cmdLine := flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
compileTemplate := cmdLine.Bool("compile-templates", false, "Compile Templates")
cmdLine.Parse(os.Args[1:])
```

### Core Variables

``` go
// Initializes the core components of the application:
//	- Router
//	- Admin: the admin dashboard / backoffice
//	- Application: the frontend + backoffice
// Note that the Application uses the Admin to setup the glue between
// the frontend and the backend. Resources will be added directly
// to the Application object.
var (
    Router = chi.NewRouter()
    Admin  = admin.New(&admin.AdminConfig{
        SiteName: "QOR DEMO",
        Auth:     auth.AdminAuth{},
        DB:       db.DB.Set(publish2.VisibleMode, publish2.ModeOff).Set(publish2.ScheduleMode, publish2.ModeOff),
    })
    Application = application.New(&application.Config{
        Router: Router,
        Admin:  Admin,
        DB:     db.DB,
    })
)
```

### Setting Up The Server Handlers

``` go
// Add the admin views to the funcmap.
// The views are located in /app/views/auth, as configured
// in /config/auth/auth.go
funcmapmaker.AddFuncMapMaker(auth.Auth.Config.Render)

// Setup the router middlewares. Check their documentation (f12)
// to see more details about each middleware.
Router.Use(middleware.SomeMiddlWare)
...

// Add components to the application.
// Each component uses the New() and Config{} to easily
// be used by the core application.
Application.Use(component.New(&component.Config{}))
...

// Configure the static folders, routing to custom paths
Application.Use(static.New(&static.Config{
    Prefixs: []string{"/system"},
    Handler: utils.FileServer(http.Dir(filepath.Join(config.Root, "public"))),
}))
...

// If the --compile-templates flag was added:
if *compileTemplate {
    bindatafs.AssetFS.Compile()
} else {
    // Otherwise, setup the server (ommited details for brevity)
    http.ListenAndServe(...)
}
```