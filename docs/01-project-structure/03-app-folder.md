## App folder

### Implementation Requirements Of New Components
Every component must implement the empty struct `Config` as well as the `New(config *Config)` method.

If you remember from the root main.go overview, we used the `Application.Use(model.New(&model.Config{})` to add the components to the application.

``` go
// This struct can be named as you want.
type App struct {
    Config *Config
}

// Config is just an empty struct, that will be populated later on
// in the ConfigureAdmin or the ConfigureApplication methods.
type Config struct {
}

// New wires the customized Config struct to the App struct.
func New(config *Config) *App {
    return &App{Config: config}
}

```

### Configuring The Component For The Admin Dashboard
* Official docs: [Resources](https://github.com/qor/doc/blob/master/admin/resources.md), [Extend Admin](https://github.com/qor/doc/blob/master/admin/extend_admin.md)

If you want to add a new manageable component to the QOR Admin dashboard, you need to implement the ``ConfigureAdmin()` method and add your component (Model) as a resource and/or as a navigable menu item.

Here as an example of how to add the "pages" resource to the admin dashboard:

``` go
func (App) ConfigureAdmin(Admin *admin.Admin) {
    // Adds the "Page Management" menu item to the admin dashboard.
    // The "Priority" sets the position in the menu.
    Admin.AddMenu(&admin.Menu{Name: "Pages Management", Priority: 4})

    // Add the "Post" model into the just created menu item,
    // so we can manage (create, edit, remove) new posts
    article := Admin.AddResource(&blogs.Post{}, &admin.Config{Menu: []string{"Pages Management"}})

    // As an extra step, you can set what model's fields will be 
    // shown in the main page of the "Posts" menu
    article.IndexAttrs("ID", "VersionName", "ScheduledStartAt", "ScheduledEndAt", "Author", "Title")
}
```

### Configuring The Component For The Application
The `ConfigureApplication()` method has two main goals:
* Calling the `ConfigureAdmin()` method
* Adding a public route to publicly access the component

Following the same example as before, here is how we would implement the `ConfigureApplication()` method to configure the "pages" resource:

``` go
func (app App) ConfigureApplication(application *application.Application) {

    // As this is a public resource, we also need to let the application know
    // where the views are located. This is done creating a new controller and
    // assigning it a renderer.
    controller := &Controller{
        View: render.New(
            &render.Config{
                AssetFileSystem: application.AssetFS.NameSpace("blog"),
            },
            "app/pages/views",
        ),
    }
    funcmapmaker.AddFuncMapMaker(controller.View)

    // Call the ConfigureAdmin() method on this resource
    app.ConfigureAdmin(application.Admin)

    // Create a new route pointing to the controller's Index method,
    // which is created in the /app/pages/handlers.go file.
    application.Router.Get("/blog", controller.Index)
}
```
