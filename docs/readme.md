# Official Docs
* https://github.com/qor/doc
* https://github.com/qor/qor

## Flags
* QUESTION --> a question that needs to be solved!
* TODO --> things I would like to include in the project to reinforce the docs understanding


## TO DO's
* How to attach medias to posts? (a media picker to select the post's image)
    * Check the Product implementation from the qor-example app, where a single/multiple media pickers are used