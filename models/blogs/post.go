package blogs

import (
	"github.com/jinzhu/gorm"
	"github.com/qor/publish2"

	"bitbucket.org/tramascrew/qor-blog/models/users"
)

type Post struct {
	gorm.Model
	Author   users.User
	AuthorID uint
	Title    string
	Content  string `gorm:"type:text"`
	publish2.Version
	publish2.Schedule
	publish2.Visible
}
