package account

import (
	"net/http"

	"github.com/qor/render"

	"bitbucket.org/tramascrew/qor-blog/utils"
)

// Controller products controller
type Controller struct {
	View *render.Render
}

// Profile profile show page
func (ctrl Controller) Profile(w http.ResponseWriter, req *http.Request) {
	var (
		currentUser = utils.GetCurrentUser(req)
		// tx          = utils.GetDB(req)
	)

	ctrl.View.Execute("profile", map[string]interface{}{
		"CurrentUser": currentUser,
	}, req, w)
}

// Update update profile page
func (ctrl Controller) Update(w http.ResponseWriter, req *http.Request) {
	// FIXME
}
