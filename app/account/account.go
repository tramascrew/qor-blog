package account

import (
	"fmt"

	"github.com/go-chi/jwtauth"
	"github.com/qor/admin"
	"github.com/qor/qor"
	"github.com/qor/qor/resource"
	qorutils "github.com/qor/qor/utils"
	"github.com/qor/validations"
	"golang.org/x/crypto/bcrypt"

	"bitbucket.org/tramascrew/qor-blog/config"
	"bitbucket.org/tramascrew/qor-blog/config/application"
	"bitbucket.org/tramascrew/qor-blog/config/auth"
	"bitbucket.org/tramascrew/qor-blog/models/users"
)

// New new home app
func New(config *Config) *App {
	return &App{Config: config}
}

// App home app
type App struct {
	Config *Config
}

// Config home config struct
type Config struct {
}

// ConfigureApplication configure application
func (app App) ConfigureApplication(application *application.Application) {
	app.ConfigureAdmin(application.Admin)
	application.Router.Mount("/auth/", auth.Auth.NewServeMux())
}

// ConfigureAdmin configure admin interface
func (App) ConfigureAdmin(Admin *admin.Admin) {
	Admin.AddMenu(&admin.Menu{Name: "User Management", Priority: 3})
	user := Admin.AddResource(&users.User{}, &admin.Config{Menu: []string{"User Management"}})
	user.Meta(&admin.Meta{Name: "Role", Config: &admin.SelectOneConfig{Collection: []string{"Admin", "Editor"}}})
	user.Meta(&admin.Meta{Name: "Password",
		Type:   "password",
		Valuer: func(interface{}, *qor.Context) interface{} { return "" },
		Setter: func(resource interface{}, metaValue *resource.MetaValue, context *qor.Context) {
			if newPassword := qorutils.ToString(metaValue.Value); newPassword != "" {
				bcryptPassword, err := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
				if err != nil {
					context.DB.AddError(validations.NewError(user, "Password", "Can't encrypt password"))
					return
				}
				u := resource.(*users.User)
				u.Password = string(bcryptPassword)
			}
		},
	})
	user.Meta(&admin.Meta{Name: "Confirmed", Valuer: func(user interface{}, ctx *qor.Context) interface{} {
		if user.(*users.User).ID == 0 {
			return true
		}
		return user.(*users.User).Confirmed
	}})

	user.Filter(&admin.Filter{
		Name: "Role",
		Config: &admin.SelectOneConfig{
			Collection: []string{"Admin", "Editor"},
		},
	})

	user.IndexAttrs("ID", "Email", "Name", "Role")
	user.ShowAttrs(
		&admin.Section{
			Title: "Basic Information",
			Rows: [][]string{
				{"Name"},
				{"Email", "Password"},
				{"Avatar"},
				{"Role"},
				{"Confirmed"},
			},
		},
	)
	user.EditAttrs(user.ShowAttrs())

	user.Action(&admin.Action{
		Name: "Check API Token",
		Handler: func(actionArgument *admin.ActionArgument) error {
			return nil
		},
		Visible: func(record interface{}, context *admin.Context) bool {
			if user, ok := record.(*users.User); ok {
				if user.Role == "Admin" {
					fmt.Println("Hola admin")
					fmt.Println(generateJWT(user.Name, user.Email))
					return true
				}
			}
			return false
		},
		Modes: []string{"menu_item", "edit", "show"},
	})
}

func generateJWT(userId string, userEmail string) *jwtauth.JWTAuth {
	// Seek, verify and validate JWT tokens
	tokenAuth := jwtauth.New("HS256", []byte(config.Config.JWTSecret), nil)

	claims := jwtauth.Claims{}
	claims.SetIssuedNow()
	claims.Set("user_id", userId+userEmail)

	_, tokenString, _ := tokenAuth.Encode(claims)
	fmt.Printf("Your API Token is %s\n\n", tokenString)

	return tokenAuth
}
