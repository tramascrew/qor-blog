package pages

import (
	"github.com/qor/admin"
	"github.com/qor/page_builder"
	"github.com/qor/render"
	"github.com/qor/widget"

	"bitbucket.org/tramascrew/qor-blog/config/application"
	"bitbucket.org/tramascrew/qor-blog/config/db"
	"bitbucket.org/tramascrew/qor-blog/models/blogs"
	"bitbucket.org/tramascrew/qor-blog/utils/funcmapmaker"
)

// New new home app
func New(config *Config) *App {
	return &App{Config: config}
}

// App home app
type App struct {
	Config *Config
}

// Config home config struct
type Config struct {
}

// ConfigureApplication configure application
func (app App) ConfigureApplication(application *application.Application) {
	controller := &Controller{View: render.New(&render.Config{AssetFileSystem: application.AssetFS.NameSpace("blog")}, "app/pages/views")}
	funcmapmaker.AddFuncMapMaker(controller.View)

	app.ConfigureAdmin(application.Admin)
	application.Router.Get("/blog", controller.Index)
	// how to implement a blog https://github.com/go-chi/chi/blob/master/_examples/rest/main.go
}

// ConfigureAdmin configure admin interface
func (App) ConfigureAdmin(Admin *admin.Admin) {
	Admin.AddMenu(&admin.Menu{Name: "Pages Management", Priority: 4})

	// Blog Management
	article := Admin.AddResource(&blogs.Post{}, &admin.Config{Menu: []string{"Pages Management"}})
	article.IndexAttrs("ID", "VersionName", "ScheduledStartAt", "ScheduledEndAt", "Author", "Title")
	article.Meta(&admin.Meta{Name: "Content", Type: "rich_editor"})

	// Setup pages
	PageBuilderWidgets := widget.New(&widget.Config{DB: db.DB})
	Admin.AddResource(PageBuilderWidgets, &admin.Config{Menu: []string{"Pages Management"}})

	page := page_builder.New(&page_builder.Config{
		Admin:       Admin,
		PageModel:   &blogs.Page{},
		Containers:  PageBuilderWidgets,
		AdminConfig: &admin.Config{Name: "Pages", Menu: []string{"Pages Management"}, Priority: 1},
	})
	page.IndexAttrs("ID", "Title", "PublishLiveNow")
}
