package api

import (
	"github.com/qor/admin"

	"bitbucket.org/tramascrew/qor-blog/config/application"
	"bitbucket.org/tramascrew/qor-blog/config/db"
	"bitbucket.org/tramascrew/qor-blog/models/blogs"
	"bitbucket.org/tramascrew/qor-blog/models/users"
)

// New new home app
func New(config *Config) *App {
	if config.Prefix == "" {
		config.Prefix = "/api"
	}
	return &App{Config: config}
}

// App home app
type App struct {
	Config *Config
}

// Config home config struct
type Config struct {
	Prefix string
	Auth   admin.Auth
}

// ConfigureApplication configure application
func (app App) ConfigureApplication(application *application.Application) {
	// // API := admin.New(&qor.Config{
	// // 	DB: db.DB,
	// // })

	API := admin.New(&admin.AdminConfig{
		Auth: app.Config.Auth,
		DB:   db.DB,
	})

	API.AddResource(&users.User{})
	API.AddResource(&blogs.Page{})
	API.AddResource(&blogs.Post{})
	// User := API.AddResource(&users.User{})
	// userOrders, _ := User.AddSubResource("Orders")
	// userOrders.AddSubResource("OrderItems", &admin.Config{Name: "Items"})

	application.Router.Mount(app.Config.Prefix, API.NewServeMux(app.Config.Prefix))
}
